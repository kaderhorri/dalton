

     ************************************************************************
     *************** Dalton - An Electronic Structure Program ***************
     ************************************************************************

    This is output from DALTON (Release Dalton2013 patch 0)
   ----------------------------------------------------------------------------
    NOTE:
     
    Dalton is an experimental code for the evaluation of molecular
    properties using (MC)SCF, DFT, CI, and CC wave functions.
    The authors accept no responsibility for the performance of
    the code or for the correctness of the results.
     
    The code (in whole or part) is provided under a licence and
    is not to be reproduced for further distribution without
    the written permission of the authors or their representatives.
     
    See the home page "http://daltonprogram.org" for further information.
     
    If results obtained with this code are published,
    the appropriate citations would be both of:
     
       K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
       L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
       P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
       J. J. Eriksen, P. Ettenhuber, B. Fernandez, L. Ferrighi,
       H. Fliegl, L. Frediani, K. Hald, A. Halkier, C. Haettig,
       H. Heiberg, T. Helgaker, A. C. Hennum, H. Hettema,
       E. Hjertenaes, S. Hoest, I.-M. Hoeyvik, M. F. Iozzi,
       B. Jansik, H. J. Aa. Jensen, D. Jonsson, P. Joergensen,
       J. Kauczor, S. Kirpekar, T. Kjaergaard, W. Klopper,
       S. Knecht, R. Kobayashi, H. Koch, J. Kongsted, A. Krapp,
       K. Kristensen, A. Ligabue, O. B. Lutnaes, J. I. Melo,
       K. V. Mikkelsen, R. H. Myhre, C. Neiss, C. B. Nielsen,
       P. Norman, J. Olsen, J. M. H. Olsen, A. Osted,
       M. J. Packer, F. Pawlowski, T. B. Pedersen, P. F. Provasi,
       S. Reine, Z. Rinkevicius, T. A. Ruden, K. Ruud, V. Rybkin,
       P. Salek, C. C. M. Samson, A. Sanchez de Meras, T. Saue,
       S. P. A. Sauer, B. Schimmelpfennig, K. Sneskov,
       A. H. Steindal, K. O. Sylvester-Hvid, P. R. Taylor,
       A. M. Teale, E. I. Tellgren, D. P. Tew, A. J. Thorvaldsen,
       L. Thoegersen, O. Vahtras, M. A. Watson, D. J. D. Wilson,
       M. Ziolkowski and H. Agren.
       The Dalton quantum chemistry program system.
       WIREs Comput. Mol. Sci. 2013. doi: 10.1002/wcms.1172
    
    and
    
       Dalton, a Molecular Electronic Structure Program,
       Release DALTON2013.0 (2013), see http://daltonprogram.org
   ----------------------------------------------------------------------------

    Authors in alphabetical order (major contribution(s) in parenthesis):

  Kestutis Aidas,           Vilnius University,           Lithuania   (QM/MM)
  Celestino Angeli,         University of Ferrara,        Italy       (NEVPT2)
  Keld L. Bak,              UNI-C,                        Denmark     (AOSOPPA, non-adiabatic coupling, magnetic properties)
  Vebjoern Bakken,          University of Oslo,           Norway      (DALTON; geometry optimizer, symmetry detection)
  Radovan Bast,             KTH Stockholm                 Sweden      (DALTON installation and execution frameworks)
  Linus Boman,              NTNU,                         Norway      (Cholesky decomposition and subsystems)
  Ove Christiansen,         Aarhus University,            Denmark     (CC module)
  Renzo Cimiraglia,         University of Ferrara,        Italy       (NEVPT2)
  Sonia Coriani,            University of Trieste,        Italy       (CC module, MCD in RESPONS)
  Paal Dahle,               University of Oslo,           Norway      (Parallelization)
  Erik K. Dalskov,          UNI-C,                        Denmark     (SOPPA)
  Thomas Enevoldsen,        Univ. of Southern Denmark,    Denmark     (SOPPA)
  Janus J. Eriksen,         Aarhus University,            Denmark     (PE-MP2/SOPPA, TDA)
  Berta Fernandez,          U. of Santiago de Compostela, Spain       (doublet spin, ESR in RESPONS)
  Lara Ferrighi,            Aarhus University,            Denmark     (PCM Cubic response)
  Heike Fliegl,             University of Oslo,           Norway      (CCSD(R12))
  Luca Frediani,            UiT The Arctic U. of Norway,  Norway      (PCM)
  Bin Gao,                  UiT The Arctic U. of Norway,  Norway      (Gen1Int library)
  Christof Haettig,         Ruhr-University Bochum,       Germany     (CC module)
  Kasper Hald,              Aarhus University,            Denmark     (CC module)
  Asger Halkier,            Aarhus University,            Denmark     (CC module)
  Hanne Heiberg,            University of Oslo,           Norway      (geometry analysis, selected one-electron integrals)
  Trygve Helgaker,          University of Oslo,           Norway      (DALTON; ABACUS, ERI, DFT modules, London, and much more)
  Alf Christian Hennum,     University of Oslo,           Norway      (Parity violation)
  Hinne Hettema,            University of Auckland,       New Zealand (quadratic response in RESPONS; SIRIUS supersymmetry)
  Eirik Hjertenaes,         NTNU,                         Norway      (Cholesky decomposition)
  Maria Francesca Iozzi,    University of Oslo,           Norway      (RPA)
  Brano Jansik              Technical Univ. of Ostrava    Czech Rep.  (DFT cubic response)
  Hans Joergen Aa. Jensen,  Univ. of Southern Denmark,    Denmark     (DALTON; SIRIUS, RESPONS, ABACUS modules, London, and much more)
  Dan Jonsson,              UiT The Arctic U. of Norway,  Norway      (cubic response in RESPONS module)
  Poul Joergensen,          Aarhus University,            Denmark     (RESPONS, ABACUS, and CC modules)
  Joanna Kauczor,           Linkoeping University,        Sweden      (Complex polarization propagator (CPP) module)
  Sheela Kirpekar,          Univ. of Southern Denmark,    Denmark     (Mass-velocity & Darwin integrals)
  Wim Klopper,              KIT Karlsruhe,                Germany     (R12 code in CC, SIRIUS, and ABACUS modules)
  Stefan Knecht,            ETH Zurich,                   Switzerland (Parallel CI and MCSCF)
  Rika Kobayashi,           Australian National Univ.,    Australia   (DIIS in CC, London in MCSCF)
  Henrik Koch,              NTNU,                         Norway      (CC module, Cholesky decomposition)
  Jacob Kongsted,           Univ. of Southern Denmark,    Denmark     (Polarizable embedding, QM/MM)
  Andrea Ligabue,           University of Modena,         Italy       (CTOCD, AOSOPPA)
  Ola B. Lutnaes,           University of Oslo,           Norway      (DFT Hessian)
  Juan I. Melo,             University of Buenos Aires,   Argentina   (LRESC, Relativistic Effects on NMR Shieldings)
  Kurt V. Mikkelsen,        University of Copenhagen,     Denmark     (MC-SCRF and QM/MM)
  Rolf H. Myhre,            NTNU,                         Norway      (Cholesky, subsystems and ECC2)
  Christian Neiss,          Univ. Erlangen-Nuernberg,     Germany     (CCSD(R12))
  Christian B. Nielsen,     University of Copenhagen,     Denmark     (QM/MM)
  Patrick Norman,           Linkoeping University,        Sweden      (Cubic response and complex response in RESPONS)
  Jeppe Olsen,              Aarhus University,            Denmark     (SIRIUS CI/density modules)
  Jogvan Magnus H. Olsen,   Univ. of Southern Denmark,    Denmark     (Polarizable embedding, PE library, QM/MM)
  Anders Osted,             Copenhagen University,        Denmark     (QM/MM)
  Martin J. Packer,         University of Sheffield,      UK          (SOPPA)
  Filip Pawlowski,          Kazimierz Wielki University   Poland      (CC3)
  Thomas B. Pedersen,       University of Oslo,           Norway      (Cholesky decomposition)
  Patricio F. Provasi,      University of Northeastern,   Argentina   (Analysis of coupling constants in localized orbitals)
  Zilvinas Rinkevicius,     KTH Stockholm,                Sweden      (open-shell DFT, ESR)
  Elias Rudberg,            KTH Stockholm,                Sweden      (DFT grid and basis info)
  Torgeir A. Ruden,         University of Oslo,           Norway      (Numerical derivatives in ABACUS)
  Kenneth Ruud,             UiT The Arctic U. of Norway,  Norway      (DALTON; ABACUS magnetic properties and  much more)
  Pawel Salek,              KTH Stockholm,                Sweden      (DALTON; DFT code)
  Claire C. M. Samson       University of Karlsruhe       Germany     (Boys localization, r12 integrals in ERI)
  Alfredo Sanchez de Meras, University of Valencia,       Spain       (CC module, Cholesky decomposition)
  Trond Saue,               Paul Sabatier University,     France      (direct Fock matrix construction)
  Stephan P. A. Sauer,      University of Copenhagen,     Denmark     (SOPPA(CCSD), SOPPA prop., AOSOPPA, vibrational g-factors)
  Bernd Schimmelpfennig,    Forschungszentrum Karlsruhe,  Germany     (AMFI module)
  Kristian Sneskov,         Aarhus University,            Denmark     (QM/MM, PE-CC)
  Arnfinn H. Steindal,      UiT The Arctic U. of Norway,  Norway      (parallel QM/MM)
  K. O. Sylvester-Hvid,     University of Copenhagen,     Denmark     (MC-SCRF)
  Peter R. Taylor,          VLSCI/Univ. of Melbourne,     Australia   (Symmetry handling ABACUS, integral transformation)
  Andrew M. Teale,          University of Nottingham,     England     (DFT-AC, DFT-D)
  David P. Tew,             University of Bristol,        England     (CCSD(R12))
  Olav Vahtras,             KTH Stockholm,                Sweden      (triplet response, spin-orbit, ESR, TDDFT, open-shell DFT)
  David J. Wilson,          La Trobe University,          Australia   (DFT Hessian and DFT magnetizabilities)
  Hans Agren,               KTH Stockholm,                Sweden      (SIRIUS module, RESPONS, MC-SCRF solvation model)
 --------------------------------------------------------------------------------

     Date and time (Linux)  : Sun Sep  8 20:38:32 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 * Work memory size             :    64000000 =  488.28 megabytes.

 * Directories for basis set searches:
   1) /home/bast/DALTON-2013.0-Source/build/test_dft_pbe
   2) /home/bast/DALTON-2013.0-Source/build/basis


Compilation information
-----------------------

 Who compiled             | bast
 Host                     | lpqlx131.ups-tlse.fr
 System                   | Linux-3.8.5-201.fc18.x86_64
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | OFF
 Fortran compiler         | /usr/bin/gfortran
 Fortran compiler version | GNU Fortran (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C compiler               | /usr/bin/gcc
 C compiler version       | gcc (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 C++ compiler             | /usr/bin/g++
 C++ compiler version     | g++ (GCC) 4.7.2 20121109 (Red Hat 4.7.2-8)
 Static linking           | OFF
 Last Git revision        | f34203295a86316e27f9e7b44f9b6769c4a046c0
 Configuration time       | 2013-09-08 20:31:27.952056


   Content of the .dal input file
 ----------------------------------

**DALTON                                          
.RUN RESPONSE                                     
**WAVE FUNCTIONS                                  
.DFT                                              
 PBE                                              
! Molpro form:                                    
! GGAKey pbex=1 pw91c=1                           
*DFT INPUT                                        
.RADINT                                           
1e-9                                              
**RESPONSE                                        
*LINEAR                                           
.SINGLE RESIDUE                                   
.TRIPLET                                          
.ROOTS                                            
1 0 0 0  0 0 0 0                                  
**END OF                                          
                                                  


   Content of the .mol file
 ----------------------------

BASIS                                                                          
cc-pVTZ                                                                        
He atom with PBE functional                                                    
---------------------------                                                    
    1                                                                          
        2.    1                                                                
He    .0000     .0000     .0000                                                
                                                                               


       *******************************************************************
       *********** Output from DALTON general input processing ***********
       *******************************************************************

 --------------------------------------------------------------------------------
   Overall default print level:    0
   Print level for DALTON.STAT:    1

    HERMIT 1- and 2-electron integral sections will be executed
    "Old" integral transformation used (limited to max 255 basis functions)
    Wave function sections will be executed (SIRIUS module)
    Dynamic molecular response properties section will be executed (RESPONSE module)
 --------------------------------------------------------------------------------


   ****************************************************************************
   *************** Output of molecule and basis set information ***************
   ****************************************************************************


    The two title cards from your ".mol" input:
    ------------------------------------------------------------------------
 1: He atom with PBE functional                                             
 2: ---------------------------                                             
    ------------------------------------------------------------------------

  Atomic type no.    1
  --------------------
  Nuclear charge:   2.00000
  Number of symmetry independent centers:    1
  Number of basis sets to read;    2
  Basis set file used for this atomic type with Z =   2 :
     "/home/bast/DALTON-2013.0-Source/build/basis/cc-pVTZ"


                      SYMADD: Requested addition of symmetry
                      --------------------------------------

 Symmetry test threshold:  5.00E-06

 - molecule centered at center of mass and rotated so
   principal axes of inertia are along coordinate axes.

 Symmetry class found: D(oo,h)        

 Symmetry Independent Centres             
 ----------------------------
       2 :      0.00000000     0.00000000     0.00000000  Isotope  1

 The following elements were found:   X  Y  Z  


                         SYMGRP: Point group information
                         -------------------------------

Full point group is: D(oo,h)        
Represented as:      D2h

   * The point group was generated by:

      Reflection in the yz-plane
      Reflection in the xz-plane
      Reflection in the xy-plane

   * Group multiplication table

        |  E   C2z  C2y  C2x   i   Oxy  Oxz  Oyz
   -----+----------------------------------------
     E  |  E   C2z  C2y  C2x   i   Oxy  Oxz  Oyz
    C2z | C2z   E   C2x  C2y  Oxy   i   Oyz  Oxz
    C2y | C2y  C2x   E   C2z  Oxz  Oyz   i   Oxy
    C2x | C2x  C2y  C2z   E   Oyz  Oxz  Oxy   i 
     i  |  i   Oxy  Oxz  Oyz   E   C2z  C2y  C2x
    Oxy | Oxy   i   Oyz  Oxz  C2z   E   C2x  C2y
    Oxz | Oxz  Oyz   i   Oxy  C2y  C2x   E   C2z
    Oyz | Oyz  Oxz  Oxy   i   C2x  C2y  C2z   E 

   * Character table

        |  E   C2z  C2y  C2x   i   Oxy  Oxz  Oyz
   -----+----------------------------------------
    Ag  |   1    1    1    1    1    1    1    1
    B3u |   1   -1   -1    1   -1    1    1   -1
    B2u |   1   -1    1   -1   -1    1   -1    1
    B1g |   1    1   -1   -1    1    1   -1   -1
    B1u |   1    1   -1   -1   -1   -1    1    1
    B2g |   1   -1    1   -1    1   -1    1   -1
    B3g |   1   -1   -1    1    1   -1   -1    1
    Au  |   1    1    1    1   -1   -1   -1   -1

   * Direct product table

        | Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
   -----+----------------------------------------
    Ag  | Ag   B3u  B2u  B1g  B1u  B2g  B3g  Au 
    B3u | B3u  Ag   B1g  B2u  B2g  B1u  Au   B3g
    B2u | B2u  B1g  Ag   B3u  B3g  Au   B1u  B2g
    B1g | B1g  B2u  B3u  Ag   Au   B3g  B2g  B1u
    B1u | B1u  B2g  B3g  Au   Ag   B3u  B2u  B1g
    B2g | B2g  B1u  Au   B3g  B3u  Ag   B1g  B2u
    B3g | B3g  Au   B1u  B2g  B2u  B1g  Ag   B3u
    Au  | Au   B3g  B2g  B1u  B1g  B2u  B3u  Ag 


                                 Isotopic Masses
                                 ---------------

                           He          4.002603

                       Total mass:     4.002603 amu
                       Natural abundance: 100.000 %

 Center-of-mass coordinates (a.u.):    0.000000    0.000000    0.000000


  Atoms and basis sets
  --------------------

  Number of atom types :    1
  Total number of atoms:    1

  Basis set used is "cc-pVTZ" from the basis set library.

  label    atoms   charge   prim   cont     basis
  ----------------------------------------------------------------------
  He          1    2.0000    17    14      [6s2p1d|3s2p1d]                                    
  ----------------------------------------------------------------------
  total:      1    2.0000    17    14
  ----------------------------------------------------------------------
  Spherical harmonic basis used.

  Threshold for neglecting AO integrals:  1.00D-12


  Cartesian Coordinates (a.u.)
  ----------------------------

  Total number of coordinates:    3
  He      :     1  x   0.0000000000    2  y   0.0000000000    3  z   0.0000000000


  Symmetry Coordinates
  --------------------

  Number of coordinates in each symmetry:     0    1    1    0    1    0    0    0

  Symmetry  B3u ( 2)

    1   He    x    1

  Symmetry  B2u ( 3)

    2   He    y    2

  Symmetry  B1u ( 5)

    3   He    z    3


@ This is an atomic calculation.


  Symmetry Orbitals
  -----------------

  Number of orbitals in each symmetry:           5    2    2    1    2    1    1    0


  Symmetry  Ag ( 1)

    1     He       1s         1
    2     He       1s         2
    3     He       1s         3
    4     He       3d0       12
    5     He       3d2+      14


  Symmetry  B3u( 2)

    6     He       2px        4
    7     He       2px        7


  Symmetry  B2u( 3)

    8     He       2py        5
    9     He       2py        8


  Symmetry  B1g( 4)

   10     He       3d2-      10


  Symmetry  B1u( 5)

   11     He       2pz        6
   12     He       2pz        9


  Symmetry  B2g( 6)

   13     He       3d1+      13


  Symmetry  B3g( 7)

   14     He       3d1-      11


  No orbitals in symmetry  Au ( 8)

  Symmetries of electric field:  B3u(2)  B2u(3)  B1u(5)

  Symmetries of magnetic field:  B3g(7)  B2g(6)  B1g(4)


                     .---------------------------------------.
                     | Starting in Integral Section (HERMIT) |
                     `---------------------------------------'



    *************************************************************************
    ****************** Output from HERMIT input processing ******************
    *************************************************************************



     ************************************************************************
     ************************** Output from HERINT **************************
     ************************************************************************


 Threshold for neglecting two-electron integrals:  1.00D-12
 Number of two-electron integrals written:         776 ( 13.9% )
 Megabytes written:                              0.014

 >>>> Total CPU  time used in HERMIT:   0.00 seconds
 >>>> Total wall time used in HERMIT:   0.00 seconds


                        .----------------------------------.
                        | End of Integral Section (HERMIT) |
                        `----------------------------------'



                   .--------------------------------------------.
                   | Starting in Wave Function Section (SIRIUS) |
                   `--------------------------------------------'



 Settings for DFT calculation:
 -----------------------------

     Default thresholds:                                       1.00D-09    1.00D-10    2.00D-12
     Default threshold for number of electrons:                1.00D-03
     DFT radial quadrature accuracy and ang. expansion order:              1.00D-09  35

 *** Output from Huckel module :

     Using EWMO model:          T
     Using EHT  model:          F
     Number of Huckel orbitals each symmetry:    1    0    0    0    0    0    0    0

 EWMO - Energy Weighted Maximum Overlap - is a Huckel type method,
        which normally is better than Extended Huckel Theory.
 Reference: Linderberg and Ohrn, Propagators in Quantum Chemistry (Wiley, 1973)

 Huckel EWMO eigenvalues for symmetry :  1
           -0.918000

 **********************************************************************
 *SIRIUS* a direct, restricted step, second order MCSCF program       *
 **********************************************************************

 
     Date and time (Linux)  : Sun Sep  8 20:38:32 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 Title lines from ".mol" input file:
     He atom with PBE functional                                             
     ---------------------------                                             

 Print level on unit LUPRI =   2 is   0
 Print level on unit LUW4  =   2 is   5

@    Restricted, closed shell Kohn-Sham DFT calculation.

@    Time-dependent Kohn-Sham DFT calculation (TD-DFT).

 Initial molecular orbitals are obtained according to
 ".MOSTART EWMO  " input option

     Wave function specification
     ============================
@    For the wave function of type :      >>> KS-DFT <<<
@    Number of closed shell electrons           2
@    Number of electrons in active shells       0
@    Total charge of the molecule               0

@    Spin multiplicity and 2 M_S                1         0
     Total number of symmetries                 8
@    Reference state symmetry                   1
 
     This is a DFT calculation of type: PBE
 Weighted mixed functional:
                      pbex:    1.00000
                      PBEC:    1.00000

     Orbital specifications
     ======================
     Abelian symmetry species          All |    1    2    3    4    5    6    7    8
                                       --- |  ---  ---  ---  ---  ---  ---  ---  ---
     Total number of orbitals           14 |    5    2    2    1    2    1    1    0
     Number of basis functions          14 |    5    2    2    1    2    1    1    0

      ** Automatic occupation of RKS orbitals **

      -- Initial occupation of symmetries is determined from extended Huckel guess.           
      -- Initial occupation of symmetries is :
@    Occupied SCF orbitals               1 |    1    0    0    0    0    0    0    0

     Maximum number of Fock   iterations      0
     Maximum number of DIIS   iterations     60
     Maximum number of QC-SCF iterations     60
     Threshold for SCF convergence     1.00D-05
 
     This is a DFT calculation of type: PBE
 Weighted mixed functional:
                      pbex:    1.00000
                      PBEC:    1.00000


 >>>>> DIIS optimization of Hartree-Fock <<<<<

 C1-DIIS algorithm; max error vectors =    4

 Automatic occupation of symmetries with   2 electrons.

 Iter     Total energy    Error norm  Delta(E)    SCF occupation
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -1.055476614964   2.0000000000   -1.70D-11
@  1  -2.89140596618       1.17D-01  -2.89D+00     1   0   0   0   0   0   0   0
      Virial theorem: -V/T =      2.007679
@      MULPOP He      0.00; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -1.044478700072   2.0000000000   -2.01D-11
@  2  -2.89210978672       2.22D-02  -7.04D-04     1   0   0   0   0   0   0   0
      Virial theorem: -V/T =      2.017614
@      MULPOP He     -0.00; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -1.049033884283   2.0000000000   -1.95D-11
@  3  -2.89213071988       9.32D-03  -2.09D-05     1   0   0   0   0   0   0   0
      Virial theorem: -V/T =      2.009583
@      MULPOP He     -0.00; 
 -----------------------------------------------------------------------------
      K-S energy, electrons, error :     -1.047675956163   2.0000000000   -1.97D-11
@  4  -2.89213531497       2.25D-06  -4.60D-06     1   0   0   0   0   0   0   0

@ *** DIIS converged in   4 iterations !
@     Converged SCF energy, gradient:     -2.892135314973    2.25D-06
    - total time used in SIRFCK :              0.00 seconds


 *** SCF orbital energy analysis ***

 Only the five lowest virtual orbital energies printed in each symmetry.

 Number of electrons :    2
 Orbital occupations :    1    0    0    0    0    0    0    0

 Sym       Kohn-Sham orbital energies

  1     -0.57706758     0.45012956     4.02504411     5.83510163     5.83510163


  2      1.19300547     7.20332254

  3      1.19300547     7.20332254

  4      5.83510163

  5      1.19300547     7.20332254

  6      5.83510163

  7      5.83510163

    E(LUMO) :     0.45012956 au (symmetry 1)
  - E(HOMO) :    -0.57706758 au (symmetry 1)
  ------------------------------------------
    gap     :     1.02719714 au

 >>> Writing SIRIFC interface file <<<

 >>>> CPU and wall time for SCF :       0.243       0.245


                       .-----------------------------------.
                       | >>> Final results from SIRIUS <<< |
                       `-----------------------------------'


@    Spin multiplicity:           1
@    Spatial symmetry:            1
@    Total charge of molecule:    0

@    Final DFT energy:             -2.892135314973                 
@    Nuclear repulsion:             0.000000000000
@    Electronic energy:            -2.892135314973

@    Final gradient norm:           0.000002250886

 
     Date and time (Linux)  : Sun Sep  8 20:38:32 2013
     Host name              : lpqlx131.ups-tlse.fr                    

 (Only coefficients >0.0100 are printed.)

 Molecular orbitals for symmetry species  1
 ------------------------------------------

    Orbital         1        2        3
   1 He  :1s     1.0257  -0.5781  -4.1823
   2 He  :1s    -0.0467  -0.8359   3.9859
   3 He  :1s     0.0238   1.6823   0.4006

 Molecular orbitals for symmetry species  2
 ------------------------------------------

    Orbital         1        2
   1 He  :2px   -0.0486  -1.2169
   2 He  :2px    1.0269   0.6546

 Molecular orbitals for symmetry species  3
 ------------------------------------------

    Orbital         1        2
   1 He  :2py   -0.0486   1.2169
   2 He  :2py    1.0269  -0.6546

 Molecular orbitals for symmetry species  4
 ------------------------------------------

    Orbital         1
   1 He  :3d2-   1.0000

 Molecular orbitals for symmetry species  5
 ------------------------------------------

    Orbital         1        2
   1 He  :2pz   -0.0486  -1.2169
   2 He  :2pz    1.0269   0.6546

 Molecular orbitals for symmetry species  6
 ------------------------------------------

    Orbital         1
   1 He  :3d1+   1.0000

 Molecular orbitals for symmetry species  7
 ------------------------------------------

    Orbital         1
   1 He  :3d1-   1.0000



 >>>> Total CPU  time used in SIRIUS :      0.25 seconds
 >>>> Total wall time used in SIRIUS :      0.25 seconds

 
     Date and time (Linux)  : Sun Sep  8 20:38:32 2013
     Host name              : lpqlx131.ups-tlse.fr                    


                     .---------------------------------------.
                     | End of Wave Function Section (SIRIUS) |
                     `---------------------------------------'



                 .------------------------------------------------.
                 | Starting in Dynamic Property Section (RESPONS) |
                 `------------------------------------------------'


 ------------------------------------------------------------------------------
  RESPONSE  -  an MCSCF, MC-srDFT, DFT, and SOPPA response property program
 ------------------------------------------------------------------------------


 <<<<<<<<<< OUTPUT FROM RESPONSE INPUT PROCESSING >>>>>>>>>>




  Linear Response single residue calculation
 -------------------------------------------

 Print level                                    : IPRPP  =   2
 Maximum number of iterations for eigenval.eqs. : MAXITP =  60
 Threshold for convergence of eigenvalue eqs.   : THCPP  = 1.000D-03
 Maximum iterations in optimal orbital algorithm: MAXITO =   5

      1 Excitation energies are calculated for symmetry no.    1

 Integral transformation: Total CPU and WALL times (sec)       0.001       0.000


   SCF energy         :       -2.892135314972583
 -- inactive part     :       -2.892135314972583
 -- nuclear repulsion :        0.000000000000000


                    *****************************************
                    *** DFT response calculation (TD-DFT) ***
                    *****************************************



 >>>>>>>>>> Linear response calculation
 >>>>>>>>>> Symmetry of excitation/property operator(s)    1

 Number of excitations of this symmetry            1
 Number of response properties of this symmetry    0
 Number of C6/C8 properties of this symmetry       0


 Perturbation symmetry.     KSYMOP:       1
 Perturbation spin symmetry.TRPLET:       T
 Orbital variables.         KZWOPT:       4
 Configuration variables.   KZCONF:       0
 Total number of variables. KZVAR :       4
 Electrons in DFTMOMO:    1.99999999998030



 <<< EXCITATION ENERGIES AND TRANSITION MOMENT CALCULATION (MCTDHF) >>>

 Operator symmetry =  1; triplet =   T


 *** THE REQUESTED    1 SOLUTION VECTORS CONVERGED

 Convergence of RSP solution vectors, threshold = 1.00D-03
 ---------------------------------------------------------------
 (dimension of paired reduced space:    4)
 RSP solution vector no.    1; norm of residual   1.65D-15

 *** RSPCTL MICROITERATIONS CONVERGED


  ******************************************************************************
  *** @ Excit. operator sym 1 & ref. state sym 1 => excited state symmetry 1 ***
  ******************************************************************************



 @ Excited state no:    1 in symmetry  1
 ---------------------------------------

@ Excitation energy :  0.96400204    au
@                      26.231830     eV
@                      211573.99     cm-1
                       2530.9870     kJ / mol

@ Total energy :      -1.9281333     au


                            PBHT MO Overlap Diagnostic
                            --------------------------

  Reference: MJG Peach, P Benfield, T Helgaker, and DJ Tozer.
             J Chem Phys 128, 044118 (2008)


  The dominant contributions:

      I    A    K_IA      K_AI   <|I|*|A|> <I^2*A^2>    Weight   Contrib

      1    2 -0.707388  0.022451  0.707977  0.904244  0.532665  0.377115

@ Overlap diagnostic LAMBDA =    0.7081


 Time used in polarization propagator calculation is      0.25 CPU seconds for symmetry 1

 >>>> Total CPU  time used in RESPONSE:   0.25 seconds
 >>>> Total wall time used in RESPONSE:   0.25 seconds


                   .-------------------------------------------.
                   | End of Dynamic Property Section (RESPONS) |
                   `-------------------------------------------'

 >>>> Total CPU  time used in DALTON:   0.51 seconds
 >>>> Total wall time used in DALTON:   0.51 seconds

 
     Date and time (Linux)  : Sun Sep  8 20:38:33 2013
     Host name              : lpqlx131.ups-tlse.fr                    
